# soal-shift-sisop-modul-2-D02-2022

## Anggota Kelompok ##

NRP | Nama
---------- | ----------
5025201028 | Muhammad Abror Al Qushoyyi
5025201221 | Naufal Faadhilah
05111940000211 | Vicky Thirdian

## Lapres Modul 2

### No 1

> 1a

Mendownload dan ekstrak file characters dan weapons serta membuat direktori "gacha_gacha" dan menjadikan direktori tersebut sebagai *working directory*.

```c
void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "Anggap_ini_database_characters.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "Anggap_ini_database_weapon.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}

void unzip(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
    // this is child

        char* argv[] = {"unzip", "-q","-n","Anggap_ini_database_characters.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip",argv);
        exit(0);

    } else {
    // this is parent
        while ((wait(&status)) > 0);
        char* argv[] = {"unzip", "-q","-n","Anggap_ini_database_weapon.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip",argv);
        exit(0);
    }
}

void mkdir_gachagacha(){
    char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

void execute(){
    pid_t child;
    int status;

    child = fork();

    if(child < 0) exit(EXIT_FAILURE);

    if(child == 0){
        zip_download();
    }

    else{

        while ((wait(&status)) > 0);
        child = fork();
        if(child < 0) exit(EXIT_FAILURE);

        if(child == 0){
            mkdir_gachagacha();
        }
        else{
            while ((wait(&status)) > 0);
            
            child = fork();
            if (child < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child == 0) {
                // this is child
                unzip();

            } else {
                // this is parent
                while ((wait(&status)) > 0);

                if ((chdir("gacha_gacha")) < 0)exit(EXIT_FAILURE);
            }
```

proses *parsing json*

```c
typedef struct _object{
    char name[50];
    int rarity;
}object;

int json_count(char *path){
    DIR *dp;
    struct dirent *ep;
    int count = 0;

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp)))if(strstr(ep->d_name, ".json") != NULL) count++;

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    return count;
}

void object_get(object *temp, char* path){
    FILE *fptr;
    char buffer[5000];

    struct json_object *parsed_json, *name, *rarity;
    fptr = fopen(path, "r");
	fread(buffer, 5000, 1, fptr);
	fclose(fptr);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

	strcpy(temp->name, json_object_get_string(name));
	temp->rarity = json_object_get_int(rarity);
}

void gacha_get(object* temp, char* dir){
    DIR *dp;
    struct dirent *ep;
    char path[500];
    int index = 0;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp)))if(strstr(ep->d_name, ".json") != NULL) {
          sprintf(path, "%s/%s", dir, ep->d_name);
          object_get(&temp[index++], path);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void execute(){
    object *characters, *weapons, temp;
    FILE *fptr;
    struct tm* local;
    time_t t;
    int primogem = 79000, counter = 0;
    int char_count = json_count("characters"), weapon_count = json_count("weapons");
    char dirname[20], filename[200], objname[200];
    characters = (object*)malloc(sizeof(object)*char_count);
    weapons = (object*)malloc(sizeof(object)*weapon_count);

    gacha_get(characters, "characters");
    gacha_get(weapons, "weapons");
}
```

> 1b-c

pembuatan dan pengisian folder dan txt file yang digunakan untuk gacha dan format penamaannya

```c
if(counter%90 == 0){
    child = fork();
    if (child < 0)exit(EXIT_FAILURE);

    sprintf(dirname, "total_gacha_%d", counter+90);
    
    if (child == 0) {
        mkdir_custom(dirname);
        exit(0);
    } else while ((wait(&status)) > 0);
}
if(counter%10 == 0){
    if(counter) fclose(fptr);
    sleep(1);
    t = time(NULL);
    local = localtime(&t);
    sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt", dirname, local->tm_hour, local->tm_min, local->tm_sec, counter+10);
    fptr = fopen(filename, "w");
}
```

> 1d

proses dan ketentuan gacha

```c
while(primogem >= 160){
    if(counter%90 == 0){
        child = fork();
        if (child < 0)exit(EXIT_FAILURE);

        sprintf(dirname, "total_gacha_%d", counter+90);
        
        if (child == 0) {
            mkdir_custom(dirname);
            exit(0);
        } else while ((wait(&status)) > 0);
    }
    if(counter%10 == 0){
        if(counter) fclose(fptr);
        sleep(1);
        t = time(NULL);
        local = localtime(&t);
        sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt", dirname, local->tm_hour, local->tm_min, local->tm_sec, counter+10);
        fptr = fopen(filename, "w");
    }
    counter++;
    primogem-=160;
    if(counter&1){
        temp = characters[rand()%char_count];
        sprintf(objname, "%d_characters_%s_%d_%d", counter, temp.name, temp.rarity, primogem);
    }
    else {
        temp = weapons[rand()%weapon_count];
        sprintf(objname, "%d_weapons_%s_%d_%d", counter, temp.name, temp.rarity, primogem);
    }
    fprintf(fptr, "%s\n", objname);
}
fclose(fptr);
```

> 1e

daemon agar program memulai proses pada waktu tertentu dan proses zip file

```c
void gacha_zip(){
    char* argv[] = {"zip",
                    "-P",
                    "satuduatiga", 
                    "-r", 
                    "../not_safe_for_wibu.zip", 
                    "total_gacha_90",  
                    "total_gacha_180",  
                    "total_gacha_270",  
                    "total_gacha_360",  
                    "total_gacha_450",  
                    "total_gacha_540", 
                    NULL};
    execv("/bin/zip",argv);
    exit(0);
}

void zip_and_delete(){
    pid_t child;
    int status;

    child = fork();
    if (child < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child == 0) {
        // this is child
        gacha_zip();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        if ((chdir("..")) < 0)exit(EXIT_FAILURE);
        char *argv[] = {"rm", "-r", "gacha_gacha", NULL};
        execv("/bin/rm", argv);
        exit(0);
    }
}

int main(){
    srand(time(0));
    pid_t pid, sid;        // Variabel untuk menyimpan PID
    struct tm* local;
    time_t t;

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    /* (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    /* (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        t = time(NULL);
        local = localtime(&t);
        if(local->tm_mday == 30 &&
           local->tm_mon == 2 &&
           local->tm_hour == 4 &&
           local->tm_min == 44) execute();
        else if(local->tm_mday == 30 &&
                local->tm_mon == 2 &&
                local->tm_hour == 7 &&
                local->tm_min == 44) zip_and_delete();

        sleep(30);
    }
    

    return 0;
}
```

> cara pengerjaan dan hasil

`$ gcc soal1.c -l json-c -o soal1`

`$ ./soal1`

catatan: disarankan untuk melakukan perubahan pada fungsi main yang terkait dengan waktu menuju waktu terdekat program akan dijalankan sebelum menjalankan program.

![command](https://cdn.discordapp.com/attachments/872527165240004652/957306594348138566/unknown.png)

![hasil1](https://cdn.discordapp.com/attachments/872527165240004652/957307105109499954/unknown.png)

note: hasil berjalannya program saat tengah melakukan proses gacha

![hasil_akhir](https://media.discordapp.net/attachments/872527165240004652/957308277090320394/unknown.png)

> kendala

- saat 1a-d selesai

![yes](https://c.tenor.com/wQZS2xqp-MAAAAAC/tired-exhausted.gif)

- saat menjalankan 1a-e

![scream](https://c.tenor.com/WzaIC9WHmogAAAAC/triggered-triggered-kaguya.gif)

### No 2

> 2a

ekstraksi drakor.zip

catatan: demi kenyamanan, file drakor.zip telah didownload manual dan diupload pada sebuah google drive praktikan sendiri agar program dapat melakukan download file .zip yang dimaksud saat dijalankan

```c
void dirmake(){
    char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
    execv("/bin/mkdir", argv);
}

void zip_download(){
    char *argv[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1MdAJqTFS1AQOGjCRwfs_MXH-XLiybygy", "-O", "drakor.zip", NULL};
    execv("/usr/bin/wget", argv);
}

void unzip_drakor(){
    char *argv[] = {"unzip", "-n", "drakor.zip", "*.png", NULL};
    execv("/bin/unzip", argv);
}
```

> 2b

pembuatan folder katedori film

```c
void mkdir_custom(char* dir){
    char *argv[] = {"mkdir", "-p", dir, NULL};
    execv("/bin/mkdir", argv);
}

void split(char* filename){
    char* token1;
    int count = 0;

    token1 = strtok(filename, "_;.");
   
    while( token1 != NULL ) {
        count++;
        if(count == 3 || count == 6){
            pid_t child_id;
            int status;
            child_id = fork();
            if (child_id < 0) exit(EXIT_FAILURE);

            if (child_id == 0) {
                mkdir_custom(token1);
            } else while ((wait(&status)) > 0);
        }
        
        token1 = strtok(NULL, "_;.");
    }
}

void dircheck(){
    DIR *dp;
    struct dirent *ep;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) split(ep->d_name);

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}
```

> 2c-d

pemindahaan poster menuju folder kategori masing-masing dan merubah nama

catatan: untuk mempermudah proses 2e, nama poster saat dipindah kedalam folder kategori diubah menjadi tahun_judul_kategori.png. setelah 2e selesai, baru mengubah nama poster sesuai dengan yang diminta.

```c
void png_move(){
    DIR *dp;
    struct dirent *ep;
    FILE *fptr1, *fptr2;

    dp = opendir(".");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL) {
        char* token1, pngname[3][50], dirname[300], temp, strtemp[280];
        int count = 0;
        FILE *fptr;
        strcpy(strtemp, ep->d_name);

        token1 = strtok(strtemp, "_;.");
    
        while( token1 != NULL ) {
            count++;
            if ((count == 1 || count == 4) && strcmp(token1, "png") != 0){
                strcpy(pngname[0], token1);
            }
            else if (count == 2 || count == 5){
                strcpy(pngname[1], token1);
            }
            else if (count == 3 || count == 6){
                strcpy(pngname[2], token1);
                sprintf(dirname, "%s/%s_%s_%s.png", pngname[2], pngname[1], pngname[0], pngname[2]);
                fptr1 = fopen(ep->d_name, "rb");
                fptr2 = fopen(dirname, "wb");

                while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);

                fclose(fptr1);
                fclose(fptr2);
            }
            
            token1 = strtok(NULL, "_;.");
            }
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void genre_check(char *dir){
    DIR *dp;
    struct dirent *ep;
    int index = 0, count, status;
    char poster_name[10][280], temp[280], *token, name[280], year[280], dirname[100], temp2[290], temp3[290];
    FILE *fptr;
    pid_t child_id;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          strcpy(poster_name[index++], ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    for(int i=0; i<index-1; i++)for (int j=0; j<index-i-1; j++){
        if(strcmp(poster_name[j], poster_name[j+1]) > 0){
            strcpy(temp, poster_name[j]);
            strcpy(poster_name[j], poster_name[j+1]);
            strcpy(poster_name[j+1], temp);
        }
    }
    sprintf(dirname, "%s/data.txt", dir);
    fptr = fopen(dirname, "w");
    fprintf(fptr, "kategori : %s\n", dir);
    for(int i=0; i<index; i++) {
        strcpy(temp, poster_name[i]);
        count = 0;
        token = strtok(temp, "_");
        while(token != NULL){
            count++;
            if(count == 1) strcpy(year, token);
            else if (count == 2) strcpy(name, token);
            token = strtok(NULL, "_");
        }
        fprintf(fptr, "\nnama : %s", name);
        fprintf(fptr, "\nrilis : %s\n", year);
        sprintf(temp3, "%s/%s.png", dir, name);
        sprintf(temp2, "%s/%s", dir, poster_name[i]);
        child_id = fork();
        if (child_id < 0) exit(EXIT_FAILURE);

        if (child_id == 0) {
                char *argv[] = {"mv", temp2, temp3, NULL};
                execv("/bin/mv", argv);

        } else while ((wait(&status)) > 0);
    }
    fclose(fptr);
}
```

>2e

pembuatan `data.txt`

```c
FILE *fptr;
    pid_t child_id;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) if(strstr(ep->d_name, ".png") != NULL){
          strcpy(poster_name[index++], ep->d_name);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    for(int i=0; i<index-1; i++)for (int j=0; j<index-i-1; j++){
        if(strcmp(poster_name[j], poster_name[j+1]) > 0){
            strcpy(temp, poster_name[j]);
            strcpy(poster_name[j], poster_name[j+1]);
            strcpy(poster_name[j+1], temp);
        }
    }
    sprintf(dirname, "%s/data.txt", dir);
    fptr = fopen(dirname, "w");
    fprintf(fptr, "kategori : %s\n", dir);
    for(int i=0; i<index; i++) {
        strcpy(temp, poster_name[i]);
        count = 0;
        token = strtok(temp, "_");
        while(token != NULL){
            count++;
            if(count == 1) strcpy(year, token);
            else if (count == 2) strcpy(name, token);
            token = strtok(NULL, "_");
        }
        fprintf(fptr, "\nnama : %s", name);
        fprintf(fptr, "\nrilis : %s\n", year);
        sprintf(temp3, "%s/%s.png", dir, name);
        sprintf(temp2, "%s/%s", dir, poster_name[i]);
        child_id = fork();
        if (child_id < 0) exit(EXIT_FAILURE);

        if (child_id == 0) {
                char *argv[] = {"mv", temp2, temp3, NULL};
                execv("/bin/mv", argv);

        } else while ((wait(&status)) > 0);
    }
    fclose(fptr);
```

> cara pengerjaan dan hasil

`$ gcc soal2.c -o soal2`

`$ ./soal2`

![command](https://media.discordapp.net/attachments/872527165240004652/957309172796506242/unknown.png?width=1440&height=479)

![hasil](https://cdn.discordapp.com/attachments/872527165240004652/957309287904972931/unknown.png)

> kendala

program untuk soal 2 dibuat ketika mendekati deadline praktikum sisop sehingga struktur kode berantakan.

![speed](https://c.tenor.com/hDjuvJ04_vwAAAAC/fast-michael.gif)

### No 3

> 3a-b

pembuatan direktori dan ekstraksi animal.zip

catatan: demi kenyamanan, file animal.zip telah didownload manual dan diupload pada sebuah google drive praktikan sendiri agar program dapat melakukan download file .zip yang dimaksud saat dijalankan

```c
void mkdir_modul2(){
    char *argv[] = {"mkdir", "-p", "modul2", NULL};
    execv("/bin/mkdir", argv);
}

void folder_create(){
    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"mkdir", "-p", "darat", NULL};
        execv("/bin/mkdir", argv);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        sleep(3);
        char *argv[] = {"mkdir", "-p", "air", NULL};
        execv("/bin/mkdir", argv);
    }
}

int main(){
    pid_t child_id;
    int status;
    char dirname[100], user[20];
    strcpy(user, getenv("USER"));
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir("home")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir(user)) < 0) {
        exit(EXIT_FAILURE);
    }

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        mkdir_modul2();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        if ((chdir("modul2")) < 0) {
            exit(EXIT_FAILURE);
        }
        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }

        if (child_id == 0) {
            // this is child
            zip_download();
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child_id == 0) {
                // this is child
                unzip_animal();
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                child_id = fork();
                if (child_id < 0) {
                    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                }

                if (child_id == 0) {
                    // this is child
                    folder_create();
                }
}
```

> 3c

memisah hewan darat dan hewan air serta memindahkan pada folder masing-masing

```c
void move_file(char* dir, char* filename){
    char c, filedir1[100], filedir2[100];
    FILE *fptr1, *fptr2;

    sprintf(filedir1, "%s/%s", "animal", filename);
    fptr1 = fopen(filedir1, "rb");

    sprintf(filedir2, "%s/%s", dir, filename);
    fptr2 = fopen(filedir2, "wb");

    while(fscanf(fptr1, "%c", &c) != EOF) fprintf(fptr2, "%c", c);

    fclose(fptr1);
    fclose(fptr2);
}

void scan_folder(){
    DIR *dp;
    struct dirent *ep;
    char filename[300];

    dp = opendir("animal");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "darat") != NULL){
              move_file("darat", ep->d_name);
          }
          else if(strstr(ep->d_name, "air") != NULL){
              move_file("air", ep->d_name);
          }
          sprintf(filename, "animal/%s", ep->d_name);
          remove(filename);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

else {
    // this is parent
    while ((wait(&status)) > 0);
    
    scan_folder();

    scan_bird();

    listing();
}
```

> 3d

menghapus semua burung

```c
void scan_bird(){
    DIR *dp;
    struct dirent *ep;
    char filename[300];

    dp = opendir("darat");

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            sprintf(filename, "darat/%s", ep->d_name);
            if(strstr(ep->d_name, "bird") != NULL){
                remove(filename);
            }
        }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

else {
    // this is parent
    while ((wait(&status)) > 0);
    
    scan_folder();

    scan_bird();

    listing();
}
```

> 3e

pembuatan `list.txt` yang berisi user file, permission, dan judul file

```c
void listing(){
    DIR *dp;
    struct dirent *ep;
    char filename[300], forstat[300];
    FILE *fptr;
    struct stat fs;
    
    fptr = fopen("air/list.txt", "w");

    dp = opendir("air");

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, "list.txt") == 0) continue;
            sprintf(forstat, "air/%s", ep->d_name);
            stat(forstat, &fs);
            sprintf(filename, "%s_", getpwuid(fs.st_uid)->pw_name);
            if( fs.st_mode & S_IRUSR ) strcat(filename, "r");
            if( fs.st_mode & S_IWUSR ) strcat(filename, "w");
            if( fs.st_mode & S_IXUSR ) strcat(filename, "x");
            strcat(filename, "_");
            strcat(filename, ep->d_name);
            fprintf(fptr, "%s\n", filename);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    fclose(fptr);
}

else {
    // this is parent
    while ((wait(&status)) > 0);
    
    scan_folder();

    scan_bird();

    listing();
}

```

> cara pengerjaan dan hasil

`$ gcc soal2.c -o soal2`

`$ ./soal2`

![command](https://cdn.discordapp.com/attachments/872527165240004652/957309944938504232/unknown.png)

![hasil](https://cdn.discordapp.com/attachments/872527165240004652/957310049456386118/unknown.png)

> kendala

tidak ada, soal ~~idaman~~ paling mudah

eh, ada sih

![cursing](https://media.discordapp.net/attachments/872527165240004652/957299782513201242/kendala_no_3.png)
