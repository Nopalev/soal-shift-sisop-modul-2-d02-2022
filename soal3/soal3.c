#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <wait.h>
#include <pwd.h>

void mkdir_modul2(){
    char *argv[] = {"mkdir", "-p", "modul2", NULL};
    execv("/bin/mkdir", argv);
}

void zip_download(){
    char *argv[] = {"wget", "-q", "https://docs.google.com/uc?export=download&id=1_FpIvdzBKG_AHJkBbylIZT28ogiEdHd2", "-O", "animal.zip", NULL};
    execv("/usr/bin/wget", argv);
}

void unzip_animal(){
    char *argv[] = {"unzip", "-n", "animal.zip", NULL};
    execv("/bin/unzip", argv);
}

void folder_create(){
    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"mkdir", "-p", "darat", NULL};
        execv("/bin/mkdir", argv);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        sleep(3);
        char *argv[] = {"mkdir", "-p", "air", NULL};
        execv("/bin/mkdir", argv);
    }
}

void move_file(char* dir, char* filename){
    char c, filedir1[100], filedir2[100];
    FILE *fptr1, *fptr2;

    sprintf(filedir1, "%s/%s", "animal", filename);
    fptr1 = fopen(filedir1, "rb");

    sprintf(filedir2, "%s/%s", dir, filename);
    fptr2 = fopen(filedir2, "wb");

    while(fscanf(fptr1, "%c", &c) != EOF) fprintf(fptr2, "%c", c);

    fclose(fptr1);
    fclose(fptr2);
}

void scan_folder(){
    DIR *dp;
    struct dirent *ep;
    char filename[300];

    dp = opendir("animal");

    if (dp != NULL)
    {
      while ((ep = readdir (dp))) {
          if(strstr(ep->d_name, "darat") != NULL){
              move_file("darat", ep->d_name);
          }
          else if(strstr(ep->d_name, "air") != NULL){
              move_file("air", ep->d_name);
          }
          sprintf(filename, "animal/%s", ep->d_name);
          remove(filename);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void scan_bird(){
    DIR *dp;
    struct dirent *ep;
    char filename[300];

    dp = opendir("darat");

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            sprintf(filename, "darat/%s", ep->d_name);
            if(strstr(ep->d_name, "bird") != NULL){
                remove(filename);
            }
        }
      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

void listing(){
    DIR *dp;
    struct dirent *ep;
    char filename[300], forstat[300];
    FILE *fptr;
    struct stat fs;
    
    fptr = fopen("air/list.txt", "w");

    dp = opendir("air");

    if (dp != NULL)
    {
        while ((ep = readdir (dp))) {
            if(strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0 || strcmp(ep->d_name, "list.txt") == 0) continue;
            sprintf(forstat, "air/%s", ep->d_name);
            stat(forstat, &fs);
            sprintf(filename, "%s_", getpwuid(fs.st_uid)->pw_name);
            if( fs.st_mode & S_IRUSR ) strcat(filename, "r");
            if( fs.st_mode & S_IWUSR ) strcat(filename, "w");
            if( fs.st_mode & S_IXUSR ) strcat(filename, "x");
            strcat(filename, "_");
            strcat(filename, ep->d_name);
            fprintf(fptr, "%s\n", filename);
        }
        (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    
    fclose(fptr);
}

int main(){
    pid_t child_id;
    int status;
    char dirname[100], user[20];
    strcpy(user, getenv("USER"));
    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir("home")) < 0) {
        exit(EXIT_FAILURE);
    }
    if ((chdir(user)) < 0) {
        exit(EXIT_FAILURE);
    }

    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        mkdir_modul2();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        if ((chdir("modul2")) < 0) {
            exit(EXIT_FAILURE);
        }
        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }

        if (child_id == 0) {
            // this is child
            zip_download();
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            child_id = fork();
            if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child_id == 0) {
                // this is child
                unzip_animal();
            } else {
                // this is parent
                while ((wait(&status)) > 0);
                child_id = fork();
                if (child_id < 0) {
                    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                }

                if (child_id == 0) {
                    // this is child
                    folder_create();
                } else {
                    // this is parent
                    while ((wait(&status)) > 0);
                    
                    scan_folder();
                    
                    scan_bird();

                    listing();
                }
            }
        }
    }
    
    return 0;
}